// 1. Spaghetti code
// 2. an object literal is a comma-separated list of key-value pairs enclosed in curly braces {}. Each key-value pair consists of a property name (key) and its value, separated by a colon :
// 3. object-oriented programming (OOP)
// 4. studentOne.enroll()
// 5. True
// 6. key-value pairs can be created in different ways depending on the context, but the most common way is using object literals, which are comma-separated lists of key-value pairs enclosed in curly braces {}.
// 7. True
// 8. True
// 9. True
// 10. True



let studentOne = {
    name : 'John',
    email : 'john@mail.com',
    grades: [89, 84, 78, 88],

    login(){
        console.log(`${this.email} has logged in`);
    },
    logout(){
        console.log(`${this.email} has logged out`);
    },
    listGrades(){
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
    },
    computeAve(){
        let sum = this.grades.reduce((acc, curr) => acc + curr, 0);
        const average = sum / this.grades.length;
        return average
    },
    willPass(){
        if(this.computeAve() >= 85){
            return true
        } else {
            return false
        }
    },
    willPassWithHonors(){
        if(this.willPass() && this.computeAve() >= 90){
            return true
        } else {
            return false
        }
    }
}

let studentTwo = {
    name : 'Joe',
    email : 'joe@mail.com',
    grades: [78, 82, 79, 85],

    login(){
        console.log(`${this.email} has logged in`);
    },
    logout(){
        console.log(`${this.email} has logged out`);
    },
    listGrades(){
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
    },
    computeAve(){
        let sum = this.grades.reduce((acc, curr) => acc + curr, 0);
        const average = sum / this.grades.length;
        return average
    },
    willPass(){
        if(this.computeAve() >= 85){
            return true
        } else {
            return false
        }
    },
    willPassWithHonors(){
        if(this.willPass() && this.computeAve() >= 90){
            return true
        } else {
            return false
        }
    }
}

let studentThree = {
    name : 'Jane',
    email : 'jane@mail.com',
    grades: [87, 89, 91, 93],

    login(){
        console.log(`${this.email} has logged in`);
    },
    logout(){
        console.log(`${this.email} has logged out`);
    },
    listGrades(){
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
    },
    computeAve(){
        let sum = this.grades.reduce((acc, curr) => acc + curr, 0);
        const average = sum / this.grades.length;
        return average
    },
    willPass(){
        if(this.computeAve() >= 85){
            return true
        } else {
            return false
        }
    },
    willPassWithHonors(){
        if(this.willPass() && this.computeAve() >= 90){
            return true
        } else {
            return false
        }
    }
}

let studentFour = {
    name : 'Jessie',
    email : 'jessie@mail.com',
    grades: [91, 89, 92, 93],

    login(){
        console.log(`${this.email} has logged in`);
    },
    logout(){
        console.log(`${this.email} has logged out`);
    },
    listGrades(){
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`)
    },
    computeAve(){
        let sum = this.grades.reduce((acc, curr) => acc + curr, 0);
        const average = sum / this.grades.length;
        return average
    },
    willPass(){
        if(this.computeAve() >= 85){
            return true
        } else {
            return false
        }
    },
    willPassWithHonors(){
        if(this.willPass() && this.computeAve() >= 90){
            return true
        } else {
            return false
        }
    }
}    

const classOf1A = {
	students : [studentOne, studentTwo, studentThree, studentFour],
	countHonorStudents(){
		let count = 0
		this.students.map(student => {
			if(student.willPassWithHonors() === true){
				count = count + 1
			} 
		})
		return count
	},
	honorsPercentage(){
		return this.countHonorStudents()/this.students.length * 100
	},
	retrieveHonorStudentInfo(){
		let count = []
		this.students.map(student => {
			if(student.willPassWithHonors() === true){
				let obj = {
					email : student.email,
					aveGrade : student.computeAve()
				}
				count.push(obj)
			} 
		})
		return count
	},
	sortHonorStudentsByGradeDesc(){
		let arr = this.retrieveHonorStudentInfo()
		return arr.sort(function(a, b){return b.aveGrade - a.aveGrade})
	}
}

