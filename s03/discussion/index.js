// In js, classes can be created using the class keyword and {} 

/*

Naming convention 

Syntax : 
	class NameOfClass {

	}
*/

class Student {
	//
	constructor(name, email, grades) {

		const isWithinTheRange = (currentValue) => currentValue <= 100 && currentValue >= 0 && typeof currentValue === 'number';
		this.name = name;
		this.email = email;
		this.gradeAve =  undefined;
		this.passed = undefined;
		this.passedWithHonors = undefined;
		// Activity 1
		this.grades = grades.length === 4 && typeof grades === "object" && grades.every(isWithinTheRange)? grades : undefined
	}

	// Methods 
	login() {
		console.log(`${this.email} has logged in`);
		return this;
	}
	logout() {
		console.log(`${this.email} has logged out`);
		return this;
	}
	listGrades() {
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}.`)
		return this;
	}
	computeAve() {
		let sum = 0
		this.grades.forEach(grade => sum = sum + grade);
		// we update our property
		this.gradeAve = sum/4
		return this
	}
	willPass(){
		this.passed = this.computeAve() >= 85? true : false
		return this
    }
    willPassWithHonors(){
    	this.passedWithHonors = this.willPass() && this.computeAve() >= 90 ? true : this.willPass() && this.computeAve() < 90 ? false : undefined
    	return this
    }
}

// let studentOneTest = new Student('John', 'john@mail.com', [101, 84, 78, 88]);
// let studentTwoTest = new Student('John', 'john@mail.com', [-10, 84, 78, 88]);
// let studentThreeTest = new Student('John', 'john@mail.com', ['hello', 84, 78, 88]);
// let studentFourTest = new Student('John', 'john@mail.com', [84, 78, 88]);

let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);


class Person {

	constructor(name, age, nationality, address) {
		this.name = name;
		this.age = age >= 18 && typeof age === 'number' ? age : undefined;
		this.nationality = nationality;
		this.address = address;
	}
}

let person1 = new Person('Kris', 19, 'Filipino', 'Lapit lng');
let person2 = new Person('Kristian', '14', 'Flipino', 'jan lng');

//Acivity 1
// 1. class
// 2. class ClassName {}
// 3. new 
// 4. instantiation
// 5. constructor method

// Activity 2
// 1. No
// 2. No
// 3. Yes
// 4. getter and setter
// 5. object



// Getter and Setter 
// Best Practice dictates that we regulate access to such properties.
// Getter - retrieval
// Setter - manipulation

