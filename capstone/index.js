
class Customer {

	constructor(email){
		this.email = email;
		this.cart = new Cart();
		this.orders = [];
	}

	checkOut(){
		this.cart.length !== 0? this.orders.push({products: this.cart.contents,  totalAmount: this.cart.totalAmount}) : null
		return this
	}	
}


class Cart {

	constructor(){
		this.contents = [];
		this.totalAmount = 0;
	}

	addToCart(prod, quantity){
		let addProduct = new Product(prod.name, prod.price)
		this.contents.push({product: addProduct, quantity: quantity})
		return this;
	}

	showCartContents(){
		this.contents.map(content => {console.log(content)})
		return this;
	}

	updateProductQuantity(name, quantity){
		this.contents.find(content => {
			content.product.name === name?
			content.quantity = quantity : null
		})
		return this
	}

	clearCartContents(){
		this.contents = [];
		return this;
	}

	computeTotal(){

		let total= 0
		this.contents.forEach(content => {
			total = total + content.product.price * content.quantity
		})
		this.totalAmount = total;
		return this
	}
}

class Product {

	constructor(name, price){
		this.name = name;
		this.price = price;
		this.isActive  = true;
	}

	archive() {
		this.isActive = this.isActive === true? false : true;
		return this;
	}

	updatePrice(price) {
		this.price = price;
		return this;
	}
}

const john = new Customer(`john@mail.com`)
console.log(john)

const prodA = new Product(`soap`, 9.99)
console.log(prodA)
console.log(john.cart.addToCart(prodA, 3))
console.log(john.cart.computeTotal())
console.log(prodA.updatePrice(12.99))
console.log(prodA.archive())
console.log(john.cart.showCartContents())
console.log(john.cart.updateProductQuantity(`soap`, 5))
console.log(john.cart.clearCartContents())
console.log(john.checkOut())