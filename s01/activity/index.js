
// Quiz:
// 1. How do you create arrays in JS?
	// - We can create an array by wrapping them with "[]"
// 2. How do you access the first character of an array?
	// - We can access the first character of an array by arrayName[0]
// 3. How do you access the last character of an array?
	// - We can access the last character of an array by arrayName[arrayName.length - 1]
// 4. What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.
	// - indexOf()
// 5. What array method loops over all elements of an array, performing a user-defined function on each iteration?
	// - forEach()
// 6. What array method creates a new array with elements obtained from a user-defined function?
	// - map()
// 7. What array method checks if all its elements satisfy a given condition?
	// - every()
// 8. What array method checks if at least one of its elements satisfies a given condition?
	// - some()
// 9. True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
	// - False
// 10.True or False: array.slice() copies elements from original array and returns them as a new array.
	// - True



let students;

students = ['Jane', 'John', 'Joe', 'Jessie'];

const addToEnd = (arr, string) => {
	if(typeof string === "string"){
		arr.push(string)
		console.log(arr)
	} else {
		console.log("error - can only add strings to an array")
	}
}

addToEnd(students, "Ryan")
addToEnd(students, 45)

const addToStart = (arr, string) => {
	if(typeof string === "string"){
		arr.unshift(string)
		console.log(arr)
	} else {
		console.log("error - can only add strings to an array")
	}
}

addToStart(students, "Tess")
addToStart(students, 33)

const elementChecker = (arr, string) => {
	if(arr.length !== 0){
		console.log(true)
	} else {
		console.log("error - passed in array is empty")
	}
}

elementChecker(students, "Jane")
elementChecker([], "Jane")



const checkAllStringsEnding = (arr, string) => {

	const isAllString = (currentValue) => typeof currentValue === "string";

	const isAllEndsWithTheGivenString = (currentValue) => currentValue[currentValue.length - 1] === string
	
	if(arr.length === 0){
		console.log("error - array must NOT be empty")
	} else if (arr.every(isAllString) === false){
		console.log("error - all array elements must be strings")
	} else if (typeof string !== "string"){
		console.log("error - 2nd argument must be of data type string")
	} else if (string.length > 1){
		console.log("error - 2nd argument must be a single character")
	} else if (arr.every(isAllEndsWithTheGivenString) === true ){
		console.log(true)
	} else if (arr.every(isAllEndsWithTheGivenString) === false ){
		console.log(false)
	}
}

checkAllStringsEnding(students, "e");
checkAllStringsEnding([], "e");
checkAllStringsEnding(["Jane", 2], "e");
checkAllStringsEnding(students, "el");
checkAllStringsEnding(students, 4);

const stringLengthSorter = (arr) => {

	const isAllString = (currentValue) => typeof currentValue === "string";

	if(arr.every(isAllString) === false) {
		console.log("error - all array elements must be strings")
	} else {
		arr.sort(function(a, b){return a.length - b.length})
		console.log(arr)
	}
}

stringLengthSorter(students)
stringLengthSorter([37, "John", 39, "Jane"]);

const startsWithCounter = (arr, string) => {

	const isAllString = (currentValue) => typeof currentValue === "string";

	let count = 0;
	

	if(arr.length === 0){
		console.log("error - array must NOT be empty")
	} else if (arr.every(isAllString) === false){
		console.log("error - all array elements must be strings")
	} else if (typeof string !== "string"){
		console.log("error - 2nd argument must be of data type string")
	} else if (string.length > 1){
		console.log("error - 2nd argument must be a single character")
	} else {
		arr.forEach(student => {
			if(student[0] === string){
				count = count + 1
			} 
		})
		console.log(count)
	}
}

startsWithCounter(students, "J")


const likeFinder = (arr, string) => {

	const isAllString = (currentValue) => typeof currentValue === "string";

	if(arr.length === 0){
		console.log("error - array must NOT be empty")
	} else if (arr.every(isAllString) === false){
		console.log("error - all array elements must be strings")
	} else if (typeof string !== "string"){
		console.log("error - 2nd argument must be of data type string")
	} else {	
		console.log(arr.filter(element => element.toLowerCase().includes(string.toLowerCase())))
	}
}

likeFinder(students, "jo")


const randomPicker = (arr) => {

	let randomNumber = Math.floor(Math.random() * arr.length)

	console.log(arr[randomNumber])
}

randomPicker(students)