// alert("Hello")

let students;

students = ['jane', 'john', 'joe', 'jessie'];

console.log(students)

// Review

// push()
// unshift()
// pop()
// shift()

// slice()
// splice()

// map() - loops over items in an array and repeats a user-defined function AND returns a new array
//forEach() - loops over items in an array and repeats a user-defined function
//every() - loops of all items satisfy every condition

// Mini Activity:
	// Using forEach, check every array item in the array is divisible by 5. if  they are, log in the console 
	// "<num> is divisible by 5", if not false 

	let arrNum = [15, 20, 23, 30, 37]

	arrNum.forEach(num => {
		if(num % 5 === 0){
			console.log(`${num} is divisible by 5`)
		} else {
			console.log(false)
		}
	})

// Math Object
// JS Math Object - allows us to perform mathematical tasks on numbers.
// All methods and properties can be used without creating a Math Object first.

// Mathematical constants
	// 8 pre-defined properties which can be called via the syntax Math.property

	console.log(Math.E)
	console.log(Math.PI)
	console.log(Math.SQRT2)
	console.log(Math.SQRT1_2)
	console.log(Math.LN2)
	console.log(Math.LN10)
	console.log(Math.LOG2E)
	console.log(Math.LOG10E)

	console.log(Math.round(Math.PI));
	console.log(Math.ceil(Math.PI));
	console.log(Math.floor(Math.PI));
	console.log(Math.trunc(Math.PI));


	console.log(Math.sqrt(3))

	// lowest value in a lists of argguments
	let numVar = -100
	console.log(Math.min(-1, -2, -4, 0, 1, 2, -3, numVar))
	console.log(Math.max(-1, -2, -4, 0, 1, 2, -3, numVar))

	const myNumber = 8 ** 5;
	console.log(myNumber);

	const myNumber2 = Math.pow(8, 5);
	console.log(myNumber2)

	// Primitive Data Type
	const string = "Me";
	console.log(string)